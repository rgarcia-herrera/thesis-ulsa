# interseccion de casos - union de controles


import vcf

exm = {}
for n in range(1,5):
    exm[n] = []

    exmvcf = vcf.Reader( open( 'monosample/exm%s_novel.vcf' % n, 'r' ) )
    for v in exmvcf:
        key = "%s:%s" % (v.CHROM, v.POS)
        exm[n].append(key)





casos     = set(exm[1]).intersection( set(exm[4]) )
controles = set(exm[2]).union(        set(exm[3]) )

candidatos = casos - controles



exmvcf = vcf.Reader( open( 'monosample/exm4_novel.vcf', 'r' ) )
candidatos_vcf = vcf.Writer( open('monosample/candidatos.vcf', 'w'), exmvcf )

for v in exmvcf:
    key = "%s:%s" % (v.CHROM, v.POS)
    if key in candidatos:
        candidatos_vcf.write_record(v)

