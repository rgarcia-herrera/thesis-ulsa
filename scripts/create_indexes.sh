#!/bin/bash

REF="/path/to/reference/human_g1k_v37.fasta"
BFAST="/path/to/bfast-0.6.5a/bfast/bfast"

# Convert the reference (nucleotide space)
$BFAST fasta2brg -f $REF

# Convert the reference (color space)
$BFAST fasta2brg -f $REF -A 1

# Create the indexes
$BFAST index -n 8 -f $REF -w 14 -A 1 \
    -m 1111111111111111111111 -i 1
$BFAST index -n 8 -f $REF -w 14 -A 1 \
    -m 111110100111110011111111111 -i 2
$BFAST index -n 8 -f $REF -w 14 -A 1 \
    -m 10111111011001100011111000111111 -i 3
$BFAST index -n 8 -f $REF -w 14 -A 1 \
    -m 1111111100101111000001100011111011 -i 4
$BFAST index -n 8 -f $REF -w 14 -A 1 \
    -m 111111110001111110011111111 -i 5
$BFAST index -n 8 -f $REF -w 14 -A 1 \
    -m 11111011010011000011000110011111111 -i 6
$BFAST index -n 8 -f $REF -w 14 -A 1 \
    -m 1111111111110011101111111 -i 7
$BFAST index -n 8 -f $REF -w 14 -A 1 \
    -m 111011000011111111001111011111 -i 8
$BFAST index -n 8 -f $REF -w 14 -A 1 \
    -m 1110110001011010011100101111101111 -i 9
$BFAST index -n 8 -f $REF -w 14 -A 1 \
    -m 111111001000110001011100110001100011111 -i 10
