import vcf
import sys

# usage: get_novel.py in.vcf novel.vcf

input  = sys.argv[1]
output = sys.argv[2]


vcf_orig   = vcf.Reader( open(input, 'r') )
vcf_nuevo  = vcf.Writer( open(output, 'w'), vcf_orig )

for record in vcf_orig:
    if record.ID == None:
        vcf_nuevo.write_record(record)
