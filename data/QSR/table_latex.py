# coding: utf-8

paths = [
    '1.DinucCovariate.dat.Dinuc_hist.pdf.png',
    '1.DinucCovariate.dat.qual_diff_v_Dinuc.pdf.png',
    '1.DinucCovariate.dat.reported_qual_v_Dinuc.pdf.png',
    '1.HomopolymerCovariate.dat.Homopolymer_hist.pdf.png',
    '1.HomopolymerCovariate.dat.qual_diff_v_Homopolymer.pdf.png',
    '1.HomopolymerCovariate.dat.reported_qual_v_Homopolymer.pdf.png',
    '1.MappingQualityCovariate.dat.MappingQuality_hist.pdf.png',
    '1.MappingQualityCovariate.dat.qual_diff_v_MappingQuality.pdf.png',
    '1.MappingQualityCovariate.dat.reported_qual_v_MappingQuality.pdf.png',
    '1.PositionCovariate.dat.Position_hist.pdf.png',
    '1.PositionCovariate.dat.qual_diff_v_Position.pdf.png',
    '1.PositionCovariate.dat.reported_qual_v_Position.pdf.png',
    '1.PrimerRoundCovariate.dat.PrimerRound_hist.pdf.png',
    '1.PrimerRoundCovariate.dat.qual_diff_v_PrimerRound.pdf.png',
    '1.PrimerRoundCovariate.dat.reported_qual_v_PrimerRound.pdf.png',
    '1.QualityScoreCovariate.dat.quality_emp_hist.pdf.png',
    '1.QualityScoreCovariate.dat.quality_emp_v_stated.pdf.png',
    '1.QualityScoreCovariate.dat.quality_rep_hist.pdf.png',
]


float = """



\\begin_layout Standard
\\begin_inset Float figure
wide false
sideways false
status collapsed

\\begin_layout Plain Layout
\\begin_inset Graphics
	filename /home/rodrigo/absorto/tesis_ulsa/data/QSR/exm%d_cov_plots/%s
	width 50text%%

\end_inset


\\begin_inset Graphics
	filename /home/rodrigo/absorto/tesis_ulsa/data/QSR/exm%d_recal_cov_plots/%s
	width 50text%%

\end_inset


\end_layout

\\begin_layout Plain Layout
\\begin_inset Caption

\\begin_layout Plain Layout
Exoma %d. %s
\end_layout

\end_inset


\end_layout

\end_inset


\end_layout

"""


pagebreak = """

\\begin_layout Standard
\\begin_inset Newpage pagebreak
\\end_inset


\\end_layout

"""

j = 0

for i in (1,2,3,4,6,7,8):
    for path in paths:
        print float % (i, path, i, path, i, path)
        if (j == 2):
            print pagebreak
            j = 0
        else:
            j += 1

