BamQC report
-----------------------------------

>>>>>>> Input

     bam file = /home/rodrigo2/NatAmGenomes/MS/data/phase_I/exm1-sorted.rg.dedup.realigned.recal.bam
     outfile = /home/rodrigo2/NatAmGenomes/MS/data/phase_I/exm1qc/outside_results.txt


>>>>>>> Reference

     number of bases = 3,101,804,739 bp
     number of contigs = 84


     (reference file is not available)



>>>>>>> Globals

     number of windows = 483

     number of reads = 231,206,804
     number of mapped reads = 104,780,467 (45.32%)

     number of mapped bases = 1,606,031,246 bp
     number of sequenced bases = 1,606,031,246 bp
     number of aligned bases = 1,619,059,596 bp


>>>>>>> Mapping quality

     mean mapping quality = 177.19


>>>>>>> ACTG content

     number of A's = 445,971,085 bp (27.77%)
     number of C's = 352,376,035 bp (21.94%)
     number of T's = 452,874,708 bp (28.2%)
     number of G's = 354,809,418 bp (22.09%)
     number of N's = 0 bp (0%)

     GC percentage = 44.03%


>>>>>>> Coverage

     mean coverageData = 20.47X
     std coverageData = 26.22X

     There is a 13.03% of reference with a coverageData >= 2X
     There is a 3.82% of reference with a coverageData >= 3X
     There is a 2.26% of reference with a coverageData >= 4X
     There is a 1.77% of reference with a coverageData >= 5X
     There is a 1.52% of reference with a coverageData >= 6X
     There is a 1.36% of reference with a coverageData >= 7X
     There is a 1.25% of reference with a coverageData >= 8X
     There is a 1.16% of reference with a coverageData >= 9X
     There is a 1.09% of reference with a coverageData >= 10X
     There is a 1.02% of reference with a coverageData >= 11X
     There is a 0.97% of reference with a coverageData >= 12X
     There is a 0.91% of reference with a coverageData >= 13X
     There is a 0.87% of reference with a coverageData >= 14X
     There is a 0.83% of reference with a coverageData >= 15X
     There is a 0.79% of reference with a coverageData >= 16X
     There is a 0.75% of reference with a coverageData >= 17X
     There is a 0.72% of reference with a coverageData >= 18X
     There is a 0.69% of reference with a coverageData >= 19X
     There is a 0.66% of reference with a coverageData >= 20X
     There is a 0.63% of reference with a coverageData >= 21X
     There is a 0.61% of reference with a coverageData >= 22X
     There is a 0.58% of reference with a coverageData >= 23X
     There is a 0.56% of reference with a coverageData >= 24X
     There is a 0.54% of reference with a coverageData >= 25X
     There is a 0.51% of reference with a coverageData >= 26X
     There is a 0.49% of reference with a coverageData >= 27X
     There is a 0.48% of reference with a coverageData >= 28X
     There is a 0.46% of reference with a coverageData >= 29X
     There is a 0.44% of reference with a coverageData >= 30X
     There is a 0.42% of reference with a coverageData >= 31X
     There is a 0.41% of reference with a coverageData >= 32X
     There is a 0.39% of reference with a coverageData >= 33X
     There is a 0.38% of reference with a coverageData >= 34X
     There is a 0.36% of reference with a coverageData >= 35X
     There is a 0.35% of reference with a coverageData >= 36X
     There is a 0.34% of reference with a coverageData >= 37X
     There is a 0.33% of reference with a coverageData >= 38X
     There is a 0.31% of reference with a coverageData >= 39X
     There is a 0.3% of reference with a coverageData >= 40X
     There is a 0.29% of reference with a coverageData >= 41X
     There is a 0.28% of reference with a coverageData >= 42X
     There is a 0.27% of reference with a coverageData >= 43X
     There is a 0.26% of reference with a coverageData >= 44X
     There is a 0.25% of reference with a coverageData >= 45X
     There is a 0.25% of reference with a coverageData >= 46X
     There is a 0.24% of reference with a coverageData >= 47X
     There is a 0.23% of reference with a coverageData >= 48X
     There is a 0.22% of reference with a coverageData >= 49X
     There is a 0.21% of reference with a coverageData >= 50X
     There is a 0.21% of reference with a coverageData >= 51X
     There is a 0.2% of reference with a coverageData >= 52X


