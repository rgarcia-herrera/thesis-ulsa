PASS	Basic Statistics	exm3.bam
PASS	Per base sequence quality	exm3.bam
PASS	Per sequence quality scores	exm3.bam
PASS	Per base sequence content	exm3.bam
PASS	Per base GC content	exm3.bam
PASS	Per sequence GC content	exm3.bam
PASS	Per base N content	exm3.bam
WARN	Sequence Length Distribution	exm3.bam
WARN	Sequence Duplication Levels	exm3.bam
PASS	Overrepresented sequences	exm3.bam
PASS	Kmer Content	exm3.bam
