PASS	Basic Statistics	exm5.bam
FAIL	Per base sequence quality	exm5.bam
FAIL	Per sequence quality scores	exm5.bam
PASS	Per base sequence content	exm5.bam
WARN	Per base GC content	exm5.bam
FAIL	Per sequence GC content	exm5.bam
FAIL	Per base N content	exm5.bam
WARN	Sequence Length Distribution	exm5.bam
PASS	Sequence Duplication Levels	exm5.bam
PASS	Overrepresented sequences	exm5.bam
FAIL	Kmer Content	exm5.bam
