PASS	Basic Statistics	exm2_F3-P2.fastq
FAIL	Per base sequence quality	exm2_F3-P2.fastq
WARN	Per sequence quality scores	exm2_F3-P2.fastq
PASS	Per base sequence content	exm2_F3-P2.fastq
WARN	Per base GC content	exm2_F3-P2.fastq
PASS	Per sequence GC content	exm2_F3-P2.fastq
PASS	Per base N content	exm2_F3-P2.fastq
PASS	Sequence Length Distribution	exm2_F3-P2.fastq
PASS	Sequence Duplication Levels	exm2_F3-P2.fastq
PASS	Overrepresented sequences	exm2_F3-P2.fastq
PASS	Kmer Content	exm2_F3-P2.fastq
