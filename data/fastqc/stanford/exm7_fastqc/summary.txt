PASS	Basic Statistics	exm7.bam
PASS	Per base sequence quality	exm7.bam
PASS	Per sequence quality scores	exm7.bam
PASS	Per base sequence content	exm7.bam
WARN	Per base GC content	exm7.bam
PASS	Per sequence GC content	exm7.bam
PASS	Per base N content	exm7.bam
WARN	Sequence Length Distribution	exm7.bam
WARN	Sequence Duplication Levels	exm7.bam
PASS	Overrepresented sequences	exm7.bam
PASS	Kmer Content	exm7.bam
